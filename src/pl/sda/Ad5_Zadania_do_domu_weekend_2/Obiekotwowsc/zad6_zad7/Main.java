package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad6_zad7;

import static pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad6_zad7.Plansza.obliczOdleglosc;

public class Main {
    public static void main(String[] args) {

        Punkt punkt1 = new Punkt(5, 12);
        Punkt punkt2 = new Punkt(6, 9);
        System.out.println(punkt1.toString());
        System.out.println(punkt2.toString());

        System.out.println(obliczOdleglosc(punkt1,punkt2));;

    }
}
