package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad10;

public class Main {
    public static void main(String[] args) {

        Pracownik pracownik1 = new Pracownik("Piotr", "D'Artagan", 1500);
        Pracownik pracownik2 = new Pracownik("Kamil", "Aramis", 1400);
        Pracownik pracownik3 = new Pracownik("Kasia", "Portos", 1800);
        Pracownik pracownik4 = new Pracownik("Basia", "Aramis", 1200);

        Pracownik[] pracownicy = new Pracownik[]{pracownik1, pracownik2, pracownik3, pracownik4};
        Firma biuroRzeczyPropagandowych = new Firma(pracownicy);

        System.out.println("Miesięczny koszt to: " + biuroRzeczyPropagandowych.obliczKosztyMiesieczne());
        System.out.println("Roczny koszt to: " + biuroRzeczyPropagandowych.obliczKosztyRoczne());


        System.out.println(biuroRzeczyPropagandowych.obliczKosztFirmy(Czas.MIESIAC,9));
    }
}