package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad6_zad7;

public class Plansza {

    double odleglosc;

    public static double obliczOdleglosc(Punkt x, Punkt y) {
        return Math.sqrt(((x.x - y.x) * (x.x - y.x)) + ((x.y - y.y) * (x.y - y.y)));
    }
}
