package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad10;

public class Pracownik {

    String imie;
    String nazwisko;
    int miesieczneWynagrodzenie;

    public Pracownik(String imie, String nazwisko, int miesieczneWynagrodzenie) {
        this.imie = imie;
        this.nazwisko = nazwisko;
        this.miesieczneWynagrodzenie = miesieczneWynagrodzenie;
    }
}
