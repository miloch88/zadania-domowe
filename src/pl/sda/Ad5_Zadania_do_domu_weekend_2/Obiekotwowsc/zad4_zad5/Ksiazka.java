package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad4_zad5;

public class Ksiazka {

    String nazwa;
    String autor;
    int rokWydania;
    boolean dostepnosc = true;

    public Ksiazka(String nazwa, String autor, int rokWydania) {
        this.nazwa = nazwa;
        this.autor = autor;
        this.rokWydania = rokWydania;
    }

    @Override
    public String toString() {
        return String.format("\"%s\", %s, rok wydania: %d",nazwa,autor,rokWydania);
    }

    public void wypozyczKsiazke() {
       dostepnosc = false;
    }

    public boolean czyJestDostepna() {
        if(dostepnosc == true){
            System.out.println("Tak książka jest dosepna");
            return true;
        }else {
            System.out.println("Nie ksiązka nie jest dostępna");
            return false;
        }
    }
}
