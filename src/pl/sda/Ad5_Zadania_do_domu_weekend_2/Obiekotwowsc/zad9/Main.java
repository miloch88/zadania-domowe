package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad9;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);

        double nowaLiczba = 0, w = 0;

        do {

            System.out.println("Wpisz dwie liczby: ");

            double a = scanner.nextDouble();
            double b = scanner.nextDouble();

            Liczba liczba = new Liczba(a, b);

            System.out.println("Co chcesz zrobić? ");
            scanner.nextLine();
            String wybor = scanner.nextLine();

            switch (wybor) {
                case "+":
                    nowaLiczba = liczba.dodaj();
                    System.out.println(nowaLiczba);
                    break;
                case "-":
                    nowaLiczba = liczba.odejmij();
                    System.out.println(nowaLiczba);
                    break;
                case "*":
                    nowaLiczba = liczba.pomnoz();
                    System.out.println(nowaLiczba);
                    break;
                case "/":
                    nowaLiczba = liczba.podziel();
                    System.out.println(nowaLiczba);
                    break;
            }

            System.out.println("Czy chcesz zachować liczbę? ");
            String wybor2 = scanner.nextLine();

            switch (wybor2) {
                case "t":
                    w = nowaLiczba;
                    break;
                case "n":
                    break;
            }

        } while (w != 666);
    }
}
