package pl.sda.Ad5_Zadania_do_domu_weekend_2.Typy_wyliczeniowe.zad2;

import java.util.Locale;

public enum Figura {

    AS, KROL, DAMA, WALET, DZIESIATKA, DZIEWIATKA, OSEMKA, SIODEMKA, SZOSTKA, PIATKA, CZWORKA, TROJKA, DWOJKA;

    public String wyswietlNazwe(){
        String nazwa = this.toString();
        nazwa = nazwa.substring(0,1).toUpperCase() + nazwa.toLowerCase().substring(1);
        return String.format(nazwa);
    }
}
