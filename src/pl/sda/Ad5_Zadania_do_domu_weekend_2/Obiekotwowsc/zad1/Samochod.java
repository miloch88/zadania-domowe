package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad1;

public class Samochod {
    int predkosc;
    String kolor;
    String marka;
    int rocznik;

    public Samochod(String kolor, String marka, int rocznik) {
        this.kolor = kolor;
        this.marka = marka;
        this.rocznik = rocznik;
    }

    public int przyspiesz() {
        while (predkosc < 140) {
            predkosc += 20;
        } return predkosc;
    }

    @Override
    public String toString() {
        return String.format("%s %s rocznik %d", kolor, marka, rocznik);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Samochod){
            Samochod ten = (Samochod)obj;
            if(this.kolor == ten.kolor || this.marka == ten.marka || this.marka == ten.marka)return true;
        }return false;
    }
}
