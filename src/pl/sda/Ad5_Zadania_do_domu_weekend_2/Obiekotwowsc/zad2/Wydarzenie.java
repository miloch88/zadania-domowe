package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad2;

import static java.lang.Math.abs;

public class Wydarzenie extends MojaData{
    String nazwaWydarzenia;
    MojaData dataWydarzenia;

    public Wydarzenie(int dzien, int miesiac, int rok) {
        super(dzien, miesiac, rok);
    }


    public int ilePozostaloLat(Wydarzenie data2) {
        return data2.rok - this.rok;
    }

    public int ilePozostaloMiesiecy(Wydarzenie data2) {
        return (data2.miesiac) + (12- this.miesiac);
    }
}


