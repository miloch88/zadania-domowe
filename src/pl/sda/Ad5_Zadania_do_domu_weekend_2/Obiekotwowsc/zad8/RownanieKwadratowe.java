package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad8;

public class RownanieKwadratowe {

    double a;
    double b;
    double c;
    double delta;


    public RownanieKwadratowe(double a, double b, double c) {
        this.a = a;
        this.b = b;
        this.c = c;
    }

public void  obliczDelte(){
    delta = (b * b) - (4 * a * c);

}

    public double obliczX1() {
        return (-b + Math.sqrt(delta))/(2*a);
    }

    public double obliczX2() {
        return (-b - Math.sqrt(delta))/(2*a);
    }

}
