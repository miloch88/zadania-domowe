package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad4_zad5;

public class Main {
    public static void main(String[] args) {

        Ksiazka ksiazka1 = new Ksiazka("Krzyżacy", "Henryk Sienkiewicz", 1900);
        Ksiazka ksiazka2 = new Ksiazka("Pan Tadeusz", "Adam Mickiewicza" , 1834);
        System.out.println(ksiazka1.toString());
        System.out.println(ksiazka2.toString());

        Ksiazka[] biblioteka = new Ksiazka[]{ksiazka1,ksiazka2};

        Biblioteka bibliotekaPubliczna = new Biblioteka();
        bibliotekaPubliczna.ksiazki = biblioteka;

        //Stan bibilioteki na początku
        System.out.println("Dostępne książki to: ");
        bibliotekaPubliczna.wyswietlDostepneKsiazki();
        System.out.println("Niedostępne książki to: ");
        bibliotekaPubliczna.wyswietlWypożyczoneKsiazki();

        ksiazka1.wypozyczKsiazke();

        //Stan biblioteki po wypożyczneiu Krzyżaków
        System.out.println("Dostępne książki to: ");
        bibliotekaPubliczna.wyswietlDostepneKsiazki();
        System.out.println("Niedostępne książki to: ");
        bibliotekaPubliczna.wyswietlWypożyczoneKsiazki();

        ksiazka1.czyJestDostepna();
        ksiazka2.czyJestDostepna();

    }

}
