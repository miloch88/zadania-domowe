package pl.sda.Ad2_Petle.zad2;

import java.util.Scanner;

public class zbior3_zad2 {
    public static void main(String[] args){

        System.out.println("Wpisz ilość powtórzeń: ");
        Scanner scanner = new Scanner(System.in);
        int iloscPowtorzen = scanner.nextInt();

        System.out.println("Pętlą for: ");
        for (int i = 0; i < iloscPowtorzen; i++) {
            System.out.println("Hello World!");
        }
        System.out.println();

        System.out.println("Pętlą while: ");
        int i = 0;
        while(i<iloscPowtorzen) {
            System.out.println("Hello World!");
            i++;
        }
    }
}
