package pl.sda.Ad5_Zadania_do_domu_weekend_2.Typy_wyliczeniowe.zad2;

public enum Kolor {

    //znaczki kart: https://pl.wikipedia.org/wiki/Kolor_(karty_do_gry)
    KIER("♥"), KARO("♦"), PIK("♠"), TREFL("♣");

    String symbol;

    Kolor(String symbol) {
        this.symbol = symbol;
    }

    public String getSymbol() {
        return symbol;
    }
}
