package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad10;

public class Firma {

    static Pracownik[] pracownicy;

    Firma(Pracownik[] pracownicy) {
        this.pracownicy = pracownicy;
    }

    int obliczKosztyMiesieczne() {
        int sumaMiesieczna = 0;
        for (Pracownik x : pracownicy) {
            sumaMiesieczna += x.miesieczneWynagrodzenie;
        }
        return sumaMiesieczna;
    }

    int obliczKosztyRoczne() {
        int kosztRoczny = 0;
        return kosztRoczny = 12 * obliczKosztyMiesieczne();

    }



    int obliczKosztFirmy(Czas czas, int jednostka) {
        int koszt = 0;
        switch (czas){
            case ROK:
                koszt = jednostka * obliczKosztyRoczne();
            case DZIEN:
                koszt = jednostka*obliczKosztyMiesieczne()/30;
            case MIESIAC:
                koszt = jednostka*obliczKosztyMiesieczne();
        }
        return koszt;
    }

}


