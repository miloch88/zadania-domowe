package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad15;

import java.util.Arrays;

public class Lista {

    int[] liczby;
    int pojemnosc;
    int rozmiar = 0;


    public Lista(int pojemnosc) {
        liczby = new int[pojemnosc];
    }

    public void dodajPozycje(int liczba) {
        liczby[rozmiar++] = liczba;
    }

    public int znajdz(int liczba) {
        for (int i = 0; i < liczby.length; i++) {
            if (liczba == liczby[i]) {
                return i;
            }
        }
        return -1;
    }

//      Nie do końca podoba mi się to nadpisanie
//    @Override
//    public String toString() {
//        String nazwa = "";
//        for (int a : liczby) {
//            if (a != 0) {
//                nazwa += a;
//            }
//        }
//            return String.valueOf(nazwa);
//    }

    public void wyswietlListe() {
        System.out.println("Maksymalna wielkość: " + liczby.length);
        System.out.println("Ilość elementów: " + rozmiar);
        System.out.println(Arrays.toString(liczby));//        System.out.println(toString());
    }

    public void  usunPowtrozenia(){
        for (int i = 0; i < liczby.length; i++) {
            for (int j = i+1; j < liczby.length; j++) {
                if(liczby[i] == liczby[j]){
                    liczby[j] = 0;
                }
            }
        }
        System.out.println(Arrays.toString(liczby));
    }


    public void odwrocTablice(){
            int[] odwroconaTablica = new int[liczby.length];
            int j = 0;
        for (int i = liczby.length-1; i>= 0  ; i--) {
            odwroconaTablica[j++] = liczby[i];
        }
            System.out.println(Arrays.toString(odwroconaTablica));;
    }
}

