package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad9;

public class Liczba {

    double a;
    double b;

    public Liczba(double a, double b) {
        this.a = a;
        this.b = b;
    }

    public double dodaj() {
        return a+b;
    }

    public double odejmij() {
        return a-b;
    }

    public double pomnoz() {
        return a*b;
    }

    public double podziel() {
        return a/b;
    }
}
