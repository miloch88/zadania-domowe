package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad1;

public class SzybkiSamochod extends Samochod {


    public SzybkiSamochod(String kolor, String marka, int rocznik) {
        super(kolor, marka, rocznik);
    }

    @Override
    public int przyspiesz() {
        while (predkosc < 200) {
            predkosc += 20;
        }
        return predkosc;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof SzybkiSamochod){
            Samochod ten = (Samochod)obj;
            if(this.kolor == ten.kolor || this.marka == ten.marka || this.marka == ten.marka)return true;
        }return false;
    }
}
