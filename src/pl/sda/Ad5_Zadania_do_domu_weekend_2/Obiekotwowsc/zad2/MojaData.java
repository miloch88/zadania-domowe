package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad2;

import java.sql.Date;

public class MojaData {
    int dzien;
    int miesiac;
    int rok;

    public MojaData(int dzien, int miesiac, int rok) {
        this.dzien = dzien;
        this.miesiac = miesiac;
        this.rok = rok;
    }

    public String wyswietlDateA(){
        return String.format("%d.%d.%d",dzien,miesiac,rok);
    }

    public String wyswietlDateB(){
        return String.format("%02d.%d.%d",dzien,miesiac,rok);
    }


//    public String wyswietlDateC(){
//
//    }
}
