package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad14;

public class Main {
    public static void main(String[] args) {

        Pozycja pozycja1 = new Pozycja("Chleb", 1, 3.5);
        Pozycja pozycja2 = new Pozycja("Cukier", 3, 4.00);
//        System.out.println(pozycja1);
//        System.out.println(pozycja2);

        Zamowienie zamowienie = new Zamowienie();

        zamowienie.dodajPozycje(pozycja1);
        zamowienie.dodajPozycje(pozycja2);

        System.out.println(zamowienie);

    }
}
