package pl.sda.Ad5_Zadania_do_domu_weekend_2.Typy_wyliczeniowe.zad2;

public class Karta {

    Kolor kolor;
    Figura figura;

    public Karta(Kolor kolor, Figura figura) {
        this.kolor = kolor;
        this.figura = figura;
    }

    @Override
    public String toString() {
        return String.format("%s %s", kolor.getSymbol(), figura.wyswietlNazwe());
    }

    public void wyswietlKarte() {
        System.out.printf("%s %s\n", figura.wyswietlNazwe(), kolor.getSymbol());
    }

    public static void main(String[] args) {

//    Karta karta = new Karta(Kolor.PIK, Figura.CZWORKA);
//    karta.wyswietlKarte();
//
//    Karta karta1 = new Karta(Kolor.KIER, Figura.KROL);
//    System.out.println(karta1.toString());

        for (Figura f : Figura.values())
            for (Kolor k : Kolor.values()) {
                System.out.printf("%s %s \n", f.wyswietlNazwe(), k.getSymbol());
            }
    }

}



