package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad13;

public class Suma {
    //konstrukotr
    public Suma() {
    }

    //metoda 1
    String suma() {
        return String.format("Pierwsza metoda, nie przeciązona...");
    }

    String suma(int a) {
        return String.format("Wpisałeś jako int a: %d.", a);
    }

    String suma(double a) {
        return String.format("Wpisałeś jako double a: %.4f.", a);
    }

    String suma(int a, int b, int c) {
        return String.format("Wpisałeś jako int a: %d, int b: %d i int c: %d.", a, b, c);
    }

    String  suma(double a, double b, double c) {
        return String.format("Wpisałeś jako double a: %f, double b: %f, double d: %f", a, b, c);
    }

    public static void main(String[] args) {

        Suma suma = new Suma();

        System.out.println(suma.suma());
        System.out.println(suma.suma(1));
        System.out.println(suma.suma(1.2345));
        System.out.println(suma.suma(1,2,3));
        System.out.println(suma.suma(1.123,4.567,8.901));

    }
}
