package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad14;

public class Pozycja {

    String nazwaTowaru;
    int iloscSztuk;
    double cenaSztuki;

    public Pozycja(String nazwaTowaru, int iloscSztuk, double cenaSztuki) {
        this.nazwaTowaru = nazwaTowaru;
        this.iloscSztuk = iloscSztuk;
        this.cenaSztuki = cenaSztuki;
    }

    double oblcziWartość(){
        return iloscSztuk*cenaSztuki;
    }

    @Override
    public String toString() {
        return  String.format("%-20s %10.2f zł %4d szt. %10.2f zł",nazwaTowaru,cenaSztuki,iloscSztuk,oblcziWartość());

    }
}
