package pl.sda.CzasoOszczedzacz;

import java.util.Scanner;
import java.util.Random;

public class Main {
    public static void main(String[] args) {

        Random random = new Random();
        int polska = random.nextInt(3);
        int kolumbia = random.nextInt(3);

        System.out.println("Wybierz swoją opcję: \"optymistyczna\", \"realistyczna\", \"pesymistyczna\" :");
        Scanner scanner = new Scanner(System.in);

        String opcja = scanner.nextLine();

        switch (opcja) {

            case "optymistyczna":
                System.out.printf("Polska - Kolumbia %d:0", polska);
                break;
            case "realistyczna":
                System.out.printf("Polska - Kolumbia %d:%d", polska, kolumbia);
                break;
            case "pesymistyczna":
                System.out.printf("Polska - Kolumbia 0:%d", kolumbia);
        }
    }
}

