package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad6_zad7;

public class Punkt {
    int x;
    int y;

    public Punkt(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return String.format("%d,%d",x,y);
    }
}
