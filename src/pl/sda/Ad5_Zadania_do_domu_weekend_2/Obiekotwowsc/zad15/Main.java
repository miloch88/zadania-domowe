package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad15;

public class Main {
    public static void main(String[] args) {

        Lista lista = new Lista(10);

        lista.dodajPozycje(9);
        lista.dodajPozycje(7);
        lista.dodajPozycje(7);
        lista.dodajPozycje(3);
        lista.dodajPozycje(1);

        System.out.println(lista.znajdz(6));

        lista.wyswietlListe();

        lista.usunPowtrozenia();

        lista.odwrocTablice();
    }
}
