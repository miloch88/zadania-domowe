package pl.sda.Ad3_Zadania_tabs_zbior.zad11;

import java.util.Arrays;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {

        int[] tablica1 = new int[1];
        int[] tablica2 = new int[1];


        tablica1 = pobierzTablice(tablica1);
        tablica2 = pobierzTablice(tablica2);

//        porownanieTablic(tablica1, tablica2);
    }

//    private static int[] porownanieTablic(int[] tablica1, int[] tablica2) {
//
////        System.out.println(Arrays.toString(tablica1));
////        System.out.println(Arrays.toString(tablica2));
//        int index = 0;
//        int[] tablica3 = new int[1];
//        for (int i = 0; i < tablica1.length; i++) {
//            for (int j = 0; j < tablica2.length; j++) {
//                if (tablica1[i] == tablica2[j]) {
//
//                    if (tablica3.length <= index) {
//                        //Poszerzamy tablicę
//                        int[] nowaTablica = new int[tablica3.length + 1];
//                        //Kopiujemy starą tablicę
//                        for (int k = 0; k < index; k++) {
//                            nowaTablica[k] = tablica1[k];
//                        }
//                        //przypisujemy nowy argument i powiększamy indeks
//                        nowaTablica[index++] = tablica1[i];
//                        tablica3 = nowaTablica;
//                    } else {
//                        tablica3[index] = tablica1[i];
//                        index++;
//                    }
//                }
//
//            }
//        }
//                System.out.println(Arrays.toString(tablica3));
//        return porownanieTablic(tablica1,tablica2);
//    }


    private static int[] pobierzTablice(int[] tablica) {
        System.out.println("Wpisz swoje liczby, aby zakończyć wpisz 0:");
        Scanner scanner = new Scanner(System.in);
        int index = 0, n;
        do {
            n = scanner.nextInt();
            if (n != 0) {
                if (tablica.length <= index) {
                    //Poszerzamy tablicę
                    int[] nowaTablica = new int[tablica.length + 1];
                    //Kopiujemy starą tablicę
                    for (int i = 0; i < index; i++) {
                        nowaTablica[i] = tablica[i];
                    }
                    //przypisujemy nowy argument i powiększamy indeks
                    nowaTablica[index++] = n;
                    tablica = nowaTablica;
                } else {
                    tablica[index] = n;
                    index++;
                }
            }
        } while (n != 0);

        System.out.println(Arrays.toString(tablica));
        return tablica;

    }

}
