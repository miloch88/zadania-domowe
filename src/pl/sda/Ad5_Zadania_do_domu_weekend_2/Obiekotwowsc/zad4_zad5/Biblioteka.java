package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad4_zad5;

public class Biblioteka {

    String imie;
    Ksiazka[] ksiazki;

    public void wyswietlDostepneKsiazki() {
        for (Ksiazka ks : ksiazki) {
            if (ks.dostepnosc) {
                System.out.println(ks);
            }
        }
    }

    public  void wyswietlWypożyczoneKsiazki() {
        for (Ksiazka ks : ksiazki) {
            if (!ks.dostepnosc) {
                System.out.println(ks);
            }
        }
    }

}



