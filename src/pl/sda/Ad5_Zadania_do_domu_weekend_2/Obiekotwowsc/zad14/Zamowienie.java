package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad14;

import java.util.Arrays;

public class Zamowienie {
    Pozycja[] pozycje;
    int maxRozmiar;
    int ileDodanych = 0;
    double sumaKoncowa = 0;

    public Zamowienie(int maxRozmiar) {
        pozycje = new Pozycja[maxRozmiar];
    }

    public Zamowienie() {
        this(10);
    }

    public void dodajPozycje(Pozycja pozycja1) {
        pozycje[ileDodanych++] = pozycja1;
    }

    double sumaKoncowa() {

        for (Pozycja p : pozycje) {
            if (p != null)
                sumaKoncowa += p.oblcziWartość();
        }
        return sumaKoncowa;
    }

    @Override
    public String toString() {
        String nazwa = "Zamówienie: \n";

        for (Pozycja p : pozycje) {
            if (p != null) {
          nazwa +=      p.toString() + "\n";
            }
        }
        nazwa += "Rzaem: " + sumaKoncowa() + " zł.";
        return String.format(nazwa);
    }
}




