package pl.sda.Ad5_Zadania_do_domu_weekend_2.Obiekotwowsc.zad1;

public class Main {
    public static void main(String[] args) {

        Samochod samochod1 = new Samochod("Czerwony", "BMW", 2015);
        Samochod samochod2 = new Samochod("Czerwony", "BMW", 2015);
        SzybkiSamochod samochod3 = new SzybkiSamochod("Czerwony", "BMW", 2015);

        System.out.println(samochod1.przyspiesz());
        System.out.println(samochod1.equals(samochod2));

        System.out.println(samochod3.equals(samochod1));

    }
}
